import jwt
from datetime import datetime, timedelta
import os

JWT_SECRET = os.environ.get('JWT_SECRET')

if not JWT_SECRET:
  raise Exception("La variable de entorno JWT_SECRET no está definida")

def object_to_dict(obj):
    data = {}
    for column in obj.__table__.columns:
        data[column.name] = getattr(obj, column.name)
    return data

class AuthService:
  def __init__(self, user_repository ):
    self.user_repository = user_repository

  def login_service(self, user_login):
    if not(user_login["password"]):
      raise Exception("password is require")

    userDB = self.user_repository.get_user_by_username(user_login['name'])
    user = object_to_dict(userDB)

    if (user_login["password"] != user["password"]):
      raise Exception("password incorrect")

    token = self.generate_token(user['email'])
    return { 'token': token, 'user': user['username'] }

  def generate_token(self, email):
    try:
      payload = {
        'user_id': email,
        'exp': datetime.now() + timedelta(days=1)# Token 1 día
      }
      token = jwt.encode(payload, JWT_SECRET, algorithm='HS256')
      return token
    except Exception as e:
      print(f"Error al generar el token: {e}")
      return None
