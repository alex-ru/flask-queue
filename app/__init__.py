#dependencias
from flask import Flask
from flask_rq2 import RQ
from multiprocessing import Process

#configuraciones
from app.config import Config
from app.utils.queues import RQManager
from app.utils.database import db

#modulos
from app.auth import auth
from app.product import product
from app.sales import sales

def start_rq_worker(queue_manager):
  queue_manager.start_worker()

def create_app():
  app = Flask(__name__)
  app.config.from_object(Config)

  #iniciar DB
  db.init_app(app)

  # Inicializar Flask-RQ2 con la aplicación Flask
  rq = RQ()
  rq.init_app(app)

  # Inicializar el gestor de colas con la instancia de RQ
  queue_manager = RQManager(rq)

  #registrar los blueprint (modulos)
  app.register_blueprint(auth)
  app.register_blueprint(product)
  app.register_blueprint(sales)

  #iniciar el worker de queue en otro proceso
  with app.app_context():
    flask_process = Process(target=start_rq_worker, args=(queue_manager,))
    flask_process.start()

  return app
