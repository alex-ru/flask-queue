from . import sales
from flask import request, jsonify
from .service import SaleService
from ..utils.middlewares import auth_jwt
import json
from .schema import CreateSaleSchema

@sales.route('/save', methods=['POST'])
#@auth_jwt
def make_sale():
  try:
    body = request.json
    sale_data = CreateSaleSchema().load(body)
    service = SaleService()
    service.save_sale(sale_data)
    return jsonify({'result': "completed"})
  except Exception as e:
    error_message = "Error creating sale: {}".format(str(e))
    return jsonify({'error': error_message}), 500