from .repository import SalesRepository
from ..product.repository import ProductRepository
from ..utils.queues import RQManager
from flask import current_app

class SaleService:
  def __init__(self):
    pass

  def save_sale(self, sale):
    queue_manager = RQManager.get_instance()
    job = queue_manager.enqueue_task(self.create_sale, sale)
    print(job)
    return job

  def create_sale(self, sale):
    print(f"Comenzando tarea: sale")
    with current_app.app_context():
      sale_repository = SalesRepository()
      product_repository = ProductRepository()
      result = sale_repository.create_sale(sale)
      sql_query = "UPDATE products SET stock = stock - 1 WHERE id = {}".format(sale["product_id"])
      product_repository.execute_query_sql(sql_query)
      return result
