from marshmallow import Schema, fields, ValidationError

class CreateSaleSchema(Schema):
    product_id = fields.Integer(required=True)
    product_category_id =fields.Integer(required=True)
    quantity = fields.Integer(required=True)
    notes = fields.Str(required=False)
    total_amount = fields.Integer(required=True)
    buyer_id =  fields.Integer(required=True)