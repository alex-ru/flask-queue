
from ..utils.database import db
from .models import Sales
# from flask import current_app

class SalesRepository:
  def get_sale_by_id(self, id):
    return Sales.query.get(id)

  def create_sale(self, sale_new):
    sale = Sales(
      notes = sale_new['notes'],
      total_amount = sale_new['total_amount'],
      quantity = sale_new['quantity'],
      buyer_id =  sale_new['buyer_id'],
      product_id = sale_new['product_id'],
      product_category_id = sale_new['product_category_id'],
    )
    db.session.add(sale)
    db.session.commit()
    return sale

  def delete_user(self, id):
    product = Sales.query.get(id)
    db.session.delete(product)
    db.session.commit()
