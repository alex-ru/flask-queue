from ..utils.database import db
from datetime import datetime

class Sales(db.Model):
    __tablename__ = 'sales'
    id = db.Column(db.Integer, primary_key=True)
    notes = db.Column(db.String(80), nullable=False)
    total_amount = db.Column(db.Integer)
    quantity = db.Column(db.Integer)
    buyer_id =  db.Column(db.Integer)
    product_id = db.Column(db.Integer)
    product_category_id = db.Column(db.Integer)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)