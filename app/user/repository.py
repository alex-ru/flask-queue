

class UserRepository:
    def get_user_by_id(self, user_id):
        from app.user.models import User
        return User.query.get(user_id)

    def get_user_by_username(self, username):
        from app.user.models import User
        return User.query.filter_by(username=username).first()

    def create_user(self, username, password):
        from app import db
        from app.user.models import User
        user = User(username=username, password=password)
        db.session.add(user)
        db.session.commit()
        return user

    def update_user(self, user_id, new_data):
        from app import db
        from app.user.models import User
        user = User.query.get(user_id)
        for key, value in new_data.items():
            setattr(user, key, value)
        db.session.commit()
        return user

    def delete_user(self, user_id):
        from app import db
        from app.user.models import User
        user = User.query.get(user_id)
        db.session.delete(user)
        db.session.commit()
