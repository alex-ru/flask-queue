from flask_rq2 import RQ
from rq import Worker
from redis import Redis

class RQManager:
    _instance = None

    def __new__(cls, rq, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
            cls._instance._initialize(rq)
        return cls._instance

    def _initialize(self, rq):
        self.redis_conn = Redis()
        self.queue = rq.get_queue()
        self.worker = Worker([self.queue], connection=self.redis_conn)

    def enqueue_task(self, task_func, *args, **kwargs):
        return self.queue.enqueue(task_func, *args, **kwargs)

    def get_queue(self):
        return self.queue

    def start_worker(self):
        self.worker.work()

    @staticmethod
    def get_instance():
        if RQManager._instance is None:
            raise RuntimeError("RQManager instance has not been initialized yet.")
        return RQManager._instance
