from flask_sqlalchemy import SQLAlchemy

class Database:
  _db = None

  def __new__(cls):
    if cls._db is None:
      cls._db = SQLAlchemy()
    return cls._db

db = Database()