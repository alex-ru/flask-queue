from flask import request, jsonify
import jwt
from functools import wraps
import os

JWT_SECRET = os.environ.get('JWT_SECRET')

if not JWT_SECRET:
  raise Exception("La variable de entorno JWT_SECRET no está definida")

# Middleware JWT
def auth_jwt(func):
  @wraps(func)
  def wrapper(*args, **kwargs):
    token = request.headers.get('Authorization')
    if not token:
      return jsonify({'message': 'Token de autorización faltante'}), 401
    try:
      payload = jwt.decode(token, JWT_SECRET, algorithms=['HS256'])
      return func(*args, **kwargs)
    except jwt.ExpiredSignatureError:
      return jsonify({'message': 'Token de autorización expirado'}), 401
    except jwt.InvalidTokenError:
      return jsonify({'message': 'Token de autorización inválido'}), 401
  return wrapper

# Middleware APIKEY
def auth_api_key(func):
  @wraps(func)
  def wrapper(*args, **kwargs):
    api_key = request.headers.get('x-api-key')
    if not api_key:
      return jsonify({'message': 'api_key de autorización faltante'}), 401
    try:
      API_KEY_SECRET = os.environ.get('API_KEY_SECRET')
      if(api_key == API_KEY_SECRET):
        return func(*args, **kwargs)
    except jwt.InvalidTokenError:
      return jsonify({'message': 'api_key de autorización inválido'}), 401
  return wrapper
