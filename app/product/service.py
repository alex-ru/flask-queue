from .repository import ProductRepository
from ..utils.queues import RQManager
from flask import current_app

class ProductService:
  def __init__(self):
    pass

  def buy_product(self, product):
    queue_manager = RQManager.get_instance()
    job = queue_manager.enqueue_task(self.create_product, {'name':'test','category':'algo'})
    print(job)
    return job

  def create_product(self, product):
    print(f"Comenzando tarea: 1")
    with current_app.app_context():
        product_repository = ProductRepository()
        print("product, ", product)
        product_repository.create_product(product)
