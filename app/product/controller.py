from . import product
from flask import request, jsonify
from .service import ProductService
from ..utils.middlewares import auth_jwt
import json

@product.route('/buy', methods=['POST'])
#@auth_jwt
def buy_product():
  try:
    body = request.get_data(as_text=True)
    product_data = json.loads(body)
    service = ProductService()
    result = service.buy_product(product_data)
    print(result)
    return jsonify({'result': 'end'})
  except Exception as e:
    error_message = "Error buying product: {}".format(str(e))
    return jsonify({'error': error_message}), 500